---
title: Summerschool dag 2
date: 2018-06-19
---

Tweede dag van de summerschool. Ik heb de doelgroep, plaats en bedrijf gekozen namelijk toeristen die komen voor kunst en cultuur, de plaats wordt amsterdam aangezien daar veel toeristen zijn en het bedrijf wordt Moco museum. 

Ik heb samen met Elliot, Sem, Thomas en Wesley onderzoek gedaan naar een concurrent namelijk de ov fiets. We zijn naar centraal gegaan om meer te weten te komen over de ov fiets, wij hebben daar ook met mensen gesproken. 

De rest van de dag heb ik deskresearch gedaan naar het Mocomuseum en een ontwerpdoel gedefineerd.