---
title: Summerschool dag 9
date: 2018-06-27
---

Wat er vandaag op de planning staat is het maken van een high-fid prototype dit moet ik al in detail uitwerken zodat dit goed getest kan worden op gebruikt niveau. Ik ben erg ver gekomen en het is zo goed als doorklikbaar. 

Ik heb in de avond een testplan geschreven voor de high-fid zodat ik dit morgen kan testen tijdens het shine moment op school. Ik heb ook besloten om door te werken tot 5 uur s’ochtends zodat ik mijn ontwerpproceskaart kan uitprinten op school en hier feedback op kan vragen voor mijn nog te behalen competentie.