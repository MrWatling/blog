---
title: Summerschool Reflectie
date: 2018-07-01
---

Ik kijk met een goede blik terug op de summerschool, ik heb hier expres voor gekozen omdat ik voor mijzelf echt wilde laten zien dat ik dit wilde. Ik wilde ook mijn inzet tonen aan de docenten dat ik toch voor de lange route kies om beter werk te leveren en dat ik daarmee een betere starrt kan schrijven. 

Ik heb veel gedaan soms te veel, ik heb vrij weinig geslapen de afgelopen weken maar ben blij met het resultaat het was druk maar wel heel erg leerzaam. Ik heb nu echt goed geleerd hoe ik mijn ontwerpproces moet inrichten en hoe ik beter kan testen doormiddel van nieuwe methodes en een goede inzet. 

Ik was het gehele jaar al erg gemotiveerd maar omdat de summerschool toch wel even hard aankomt, heb ik mij echt voor 200% procent ingezet. Ik hoop dat ik door ga naar jaar 2 want ik weet van mezelf dat ik dat kan. Ik wil nog veel leren en dieper ingaan op de materie en ik heb ook nog veel te leren. Voor de docent die dit leest bedankt voor alles en de goede feedback!!!