---
title: Summerschool dag 3
date: 2018-06-20
---

Vandaag ben ik samen met Elliott en Thomas naar amsterdam gegaan om onderzoek te doen naar mijn gekozen doelgroep. Ik ben het Moco museum in gegaan en heb daar mijn doelgroep leren kennen. Ik heb mijn doelgroep geobserveerd en ook geïnterviewd. Ik heb hier veel inzichten opgedaan en dit kan ik gebruiken voor de onderzoeks poster.

Ik heb s’avonds de onderzoeksposter gemaakt waarin alle informatie die ik heb opgedaan in amsterdam in verwerkt is.