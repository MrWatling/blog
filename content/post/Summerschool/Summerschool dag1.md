---
title: Summerschool dag 1
date: 2018-06-18
---

Vandaag de kick-off gehad van het project van de summerschool, het nieuwe project heet bike it your way. Kort gezegd ik moet een huurfiets systeem ontwerpen voor een zelf gekozen doelgroep, bedrijf en plaats dat zich moet afspelen in amsterdam. Er komen drie touchpoints aan bod een docking station, een website en een applicatie. Een van die drie moet worden uitgewerkt.

Om een doelgroep, bedrijf en plaats te kiezen heb ik een long list gemaakt hierna heb ik weggestreept welke ik niet interessant vond. Ik heb nu een aantal opties over en morgen moet een duidelijke doelgroep, plaats en bedrijf hebben gekozen