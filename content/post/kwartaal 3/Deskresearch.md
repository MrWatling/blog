---
title: Deskresearch
date: 2018-03-22
---

Vandaag mijn vrije dag in de week, ik heb vandaag het initiatief genomen om zelf deskresearch te doen nadat ik vorige week vrijdag de workshop van deskresearch heb gevolgd. Ik heb research gedaan naar hoe groot de groep is van de studenten die te maken hebben gehad met psychische klachten. Na dit stukje onderzoek heb ik op de App Store gekeken en opgezocht wat voor apps er nu al op de markt zijn die helpen bij psychische klachten of hoe je deze kan voorkomen.