---
title: Nieuwe teams
date: 2018-02-13
---

Aan het begin van design challenge 3 moesten we nieuwe teams vormen in de klas. Daarvoor heb ik een rol opgeschreven die ik graag zou willen vertolken binnen het team. Mijn eerste keus was de visual designer, dit omdat ik een visual designer ben en hier nog niet veel mee heb gedaan dit jaar. Mijn tweede rol was concepten omdat ik graag op een betere manier willen leren hoe je concepten kan bedenken. Ik ben uiteindelijk in het team gekomen met Gabi, Sam , Ricardio en Wesley

Hierna moesten wij een teamfoto gaan maken en een team naam bedenken, dit hadden wij zo bedacht en hier was iedereen het ook mee eens. We gingen naar de markthal om daar een foto te maken. 

Hierna zijn we terug gegaan naar school en hebben ik met de groep de schoolplanning doorgenomen. 