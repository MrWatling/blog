---
title: Onderzoek
date: 2018-02-18
---

Vandaag ben ik aan de slag gegaan met mijn onderzoek en heb hiervoor gekeken hoe de gezondheid van de studenten van tegenwoordig is. Ook heb ik nog twee moodboards gemaakt waarin ik aangeef wat ik gezond eten vind en ook wat ik ongezond eten vind. Dit is goed om met elkaar te vergelijken omdat iedereen een ander beeld heeft van wat gezondheid is.