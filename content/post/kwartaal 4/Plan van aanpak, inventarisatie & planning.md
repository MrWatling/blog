---
title: Plan van aanpak, inventarisatie & planning
date: 2018-04-10
---

Vandaag hebben wij met zijn allen besproken welke wijk wij zouden kiezen om vervolgend een project voor te maken. We hebben toen voor Carnisse gekozen omdat daar een aantal problemen zijn waar wij wat aan kunnen doen. Ook heb ik aan het team gevraagd welke competenties zij nog moeten halen, hierna heb ik met Ricardio gekeken welke deliverables gemaakt konden worden zodat iedereen de kans heeft om het te halen. Op basis van deze gegevens heb ik een planning gemaakt met de hulp van Ricardio zodat wij durende het project kunnen checken of wij op schema liggen.