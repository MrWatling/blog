---
title: Concept
date: 2018-04-17
---

Vandaag had ik een idee bedacht voor een concept. Het had te maken met een interactieve muur waar je verschillende dingen mee kon doen. Je kon het gebruiken om foto’s mee te bewerken maar ook om er bijvoorbeeld spelletjes mee te doen. De bedoeling is dat jongeren hiermee met een actieve en creatieve manier tot ontspanning komen. Dit idee zou je ook nog kunnen uitbreiden tot een geheel interactief park waar voor iedereen wel iets leuks bij zit. 