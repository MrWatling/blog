---
title: Expo
date: 2018-06-01
---

Vandaag was de laatste officiële lesdag van school. Vandaag ben ik begonnen met feedback formulieren maken voor de pitch. Rond 1 uur ben ik met mijn team de stand gaan opzetten voor de expositie. Hiervoor hebben wij al onze posters, een telefoon met de app, het prototype en een aantal stoeptegels gebruikt, dit om het gevoel van de straat te simuleren. Ik heb samen met wesley gepitcht omdat wij samenwerken nog moeten behalen. Nadat de belangrijke mensen waren langs geweest heeft Ricardio het overgenomen van mij zodat ik ook een rondje kon lopen. Ik ben er trots dat de expo goed is verlopen en dat wij zelfs nog 1 van de 8 winnaars waren.