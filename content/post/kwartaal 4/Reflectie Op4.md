---
title: Reflectie & terugblik
Date: 2018-06-02
---

Helaas is het jaar alweer bijna voorbij, het is super snel gegaan en ik heb veel mogen doen en toch ook veel geleerd. Als ik kijk naar hoe ik ben begonnen ben ik toch ontzettend gegroeid. In het eerste kwartaal was het voor mij echt de kat uit de boom kijken, aangezien alles nieuw voor me was na een tussenjaar. Ik kon in het begin nog wat negatief overkomen aangezien ik nog niet precies wist of de opleiding bij me paste. Hierin ben ik erg gegroeid en vinden mijn klasgenoten dat ik juist erg enthousiast ben. 

Als ik kijk naar mijn kwaliteiten aan het begin van het jaar had ik veel kennis over de programma’s en ontwerpen, dit komt omdat ik hiervoor gestudeerd heb aan het grafisch lyceum en ook een portfolio cursus heb gevolgd aan de Willem de Kooning. Waar ik minder ervaring mee had was het gehele proces. Hier ben ik enorm in gegroeid aangezien ik veel onderzoek heb kunnen doen en ook veel ideeën heb kunnen bedenken en uitwerken. Ook dankzij de workshops ben ik beter gaan begrijpen hoe ik bepaalde producten beter kan maken en hoe ik beter kan plannen. 

Ik heb nog wel veel te verbeteren volgend jaar vooral in de laatste periodes is duidelijk geworden dat ik soms nog wel wat sarcastisch of bot kan overkomen. Dit wil ik aanpakken om er echt op te letten en ook kritisch te zijn naar mezelf. Ook wil ik nog wel eens mijn zin doordrijven bij een project dit kan komen omdat ik altijd hoge eisen stel aan mijn eigen werk zodat ik dit ook kan meenemen in mijn leerdossier. Ik moet andere een kans geven want er zitten zeker vele betere ideeën tussen dan wat er uit mijn hoofd komt.