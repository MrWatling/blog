---
title: Teampresentatie maken
date: 2018-02-15
---

Vandaag aan de slag gegaan met de teampresentatie. Hiervoor maak ik de kaarten waar op staat wie welke rol heeft binnen het team. Gabi had al een grove opzet gemaakt van de kaarten, deze heb ik aangepast en ik heb ook de iconen gemaakt voor op de kaart zodat elke kaart zijn eigen rol heeft. Ik heb ook mijn stukje tekst geschreven voor op de kaart en heb deze uiteindelijk helemaal opgemaakt.